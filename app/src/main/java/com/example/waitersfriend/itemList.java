package com.example.waitersfriend;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;

import com.rdevblog.order.R;


public class itemList extends ActionBarActivity {

    private ListView OrderList;

    private ArrayAdapter<Order> listAdapter;

    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_list);

        //gets and sets the toolbar from xml to the activity
        toolbar = (Toolbar)findViewById(R.id.order_list_toolbar);
        setSupportActionBar(toolbar);

        //get the list view
        OrderList = (ListView)findViewById(R.id.listView_Order);

        //make a listview array adapter to handle items in the list and add it to the list view
       listAdapter = new ArrayAdapter<>(getApplicationContext(), R.layout.order_list_item, orderList.getInstance().getOrderArrayList());
        OrderList.setAdapter(listAdapter);

        //set the on item click listener to make a new activity with the relevant information, in this case the position of the item clicked.
        OrderList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getApplicationContext(), makeOrder.class);
                intent.putExtra("OrderFromArrayList", position);
                startActivity(intent);
            }
        });

        //Fab button to start a new order
        ImageButton imageButton = (ImageButton)findViewById(R.id.new_order_button);
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), makeOrder.class);
                intent.putExtra("OrderFromArrayList", -1);
                startActivity(intent);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main_order_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        listAdapter.notifyDataSetChanged();
    }
}
