package com.example.waitersfriend;

import java.util.ArrayList;


// An array list that contains all of the orders.

 public class orderList {
    private static orderList instanceOf = new orderList();

    public static orderList getInstance() {
        return instanceOf;
    }

    private ArrayList<Order> orderArrayList = new ArrayList<>();

    private orderList() {
    }

    public void AddOrder(Order order){
        orderArrayList.add(order);
    }

    public Order GetOrder(int position){
        return orderArrayList.get(position);
    }

    public ArrayList<Order> getOrderArrayList() {
        return orderArrayList;
    }
}
