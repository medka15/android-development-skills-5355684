package com.example.waitersfriend;

import java.util.ArrayList;


// An object for the order that is made up of the item object
public class Order {

    private ArrayList<Products> items = new ArrayList<>();
    private String nameOrder;

    public Order() {

    }

    public ArrayList<Products> getItems() {
        return items;
    }

    public void setItems(ArrayList<Products> items) {
        this.items = items;
    }

    public String getNameOrder() {
        return nameOrder;
    }

    public void setOrderName(String orderName) {
        this.nameOrder = orderName;
    }

    @Override
    public String toString() {
        return this.nameOrder;
    }
}
