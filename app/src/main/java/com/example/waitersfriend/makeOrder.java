package com.example.waitersfriend;

import android.content.Context;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.rdevblog.order.R;


//An activity for making a new order.
public class makeOrder extends ActionBarActivity {

    Toolbar toolbar;

    LinearLayout viewListViewer;

    //This method creates the view
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_order);
        // get extras from the intent if there are any
        int position = getIntent().getExtras().getInt("OrderFromArrayList");

        //get the linear layout for the items in the list
        viewListViewer = (LinearLayout)findViewById(R.id.view_list);

        //see if it's a new order or opening a previous one
        if (position !=-1){
            Order order = orderList.getInstance().GetOrder(position);

            //Add all the items into the order
            for (int i = 0; i < order.getItems().size(); i++) {
                Products item = order.getItems().get(i);
                //create an inflater to make a new view
                LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                //make the new view
                View newCard = inflater.inflate(R.layout.order_view, null);
                //sort the spinner out in the new view
                Spinner foodSpinner = (Spinner)newCard.findViewById(R.id.food_title);
                ArrayAdapter<CharSequence> itemAdapter   = ArrayAdapter.createFromResource(getApplicationContext(),
                        R.array.items, R.layout.spinner_list_item);
                itemAdapter.setDropDownViewResource(R.layout.spinner_list_item);
                foodSpinner.setAdapter(itemAdapter);

                //set the text
                EditText editText = (EditText)newCard.findViewById(R.id.price);
                editText.setText(String.valueOf(item.getPrice()));

                //add view to the linear layout
               viewListViewer.addView(newCard);

            }

        }

        //Finds the toolbar by id and set it as the action bar for the activity
        toolbar = (Toolbar)findViewById(R.id.new_order_toolbar);
        setSupportActionBar(toolbar);

        //the button for the fab
        ImageButton plusButton = (ImageButton)findViewById(R.id.new_view_button);

        //This OnclickListener is used to verify when the button is clicked.
        plusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /** adds a new view to the layout*/
                LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View newCard = inflater.inflate(R.layout.order_view, null);
                Spinner foodSpinner = (Spinner)newCard.findViewById(R.id.food_title);
                ArrayAdapter<CharSequence> itemAdapter   = ArrayAdapter.createFromResource(getApplicationContext(),
                        R.array.items, R.layout.spinner_list_item);
                itemAdapter.setDropDownViewResource(R.layout.spinner_list_item);
                foodSpinner.setAdapter(itemAdapter);

                viewListViewer.addView(newCard);
            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_order_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        // when the confirm button is pressed the order is saved.
        if (id == R.id.action_settings) {
            LinearLayout view;
            CardView itemView;
            Spinner itemTitle;
            EditText itemPrice;
            Products orderItem;
            Order anOrder = new Order();

            //Loops through all the items in the order and saves them.
            for (int i = 0; i < viewListViewer.getChildCount(); i++) {
                view = (LinearLayout)viewListViewer.getChildAt(i);
                for (int j = 0; j < view.getChildCount(); j++) {
                    itemView = (CardView)view.getChildAt(j);
                    itemTitle = (Spinner)itemView.findViewById(R.id.food_title);
                    itemPrice = (EditText)itemView.findViewById(R.id.price);
                    orderItem = new Products();
                    orderItem.setFood(itemTitle.getSelectedItem().toString());
                    try {
                        orderItem.setPrice(Float.valueOf(itemPrice.getText().toString()));
                    }catch (NumberFormatException e){
                        orderItem.setPrice(0);
                    }
                    anOrder.getItems().add(orderItem);
                }
            }
            //make and add the order to the array
            anOrder.setOrderName("Order: "+String.valueOf(orderList.getInstance().getOrderArrayList().size()+1));
            orderList.getInstance().AddOrder(anOrder);

            //close the activity
            finish();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
