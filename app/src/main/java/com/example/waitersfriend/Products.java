package com.example.waitersfriend;

// An object for the items in an order
public class Products {

    private String food;
    private float price;

    public Products() {

    }

    public String getFood() {
        return food;
    }

    public void setFood(String food) {
        this.food = food;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float Price) {
        this.price = Price;
    }
}
