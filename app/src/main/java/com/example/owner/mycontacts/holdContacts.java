package com.example.owner.mycontacts;

//stores the contacts
public class holdContacts
{
    public int iD;
    public String name;
    public String Surname;
    public String phoneNumber;

    // constructor
    public holdContacts(String Name, String surname, String phoneNumber)
    {
        this.name = Name;
        this.Surname = surname;
        this.phoneNumber = phoneNumber;
    }

    public holdContacts(int ID, String Name, String surname, String phoneNumber) {
        this.iD = ID;
        this.name = Name;
        this.Surname = surname;
        this.phoneNumber = phoneNumber;

    }

    // gets and sets the variables
    public int getID() {
        return iD;
    }

    public void setID(int id) {
        this.iD = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String Name) {
        this.name = Name;
    }

    public String getSurname() {
        return Surname;
    }

    public void setSurname(String surname) {
        this.Surname = surname;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }



}
