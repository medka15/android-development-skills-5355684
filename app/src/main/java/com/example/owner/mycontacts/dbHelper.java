package com.example.owner.mycontacts;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

public class dbHelper extends SQLiteOpenHelper
{
   private static final int DATABASE_VERSION = 1;

    // creates columns and rows
   private static final String DATABASE_NAME = "contactManager",TABLE_CONTACTS = "contacts",KEY_ID = "id",KEY_NAME = "name",KEY_SURNAME = "Surname",KEY_PHONE = "phone";


    //constructor
    public dbHelper(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    //Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db)
    {
        String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_CONTACTS + "(" + KEY_ID + "INTEGER PRIMARY KEY AUTOINCREMENT," + KEY_NAME + " TEXT," + KEY_SURNAME + " TEXT," + KEY_PHONE + " TEXT)";
        db.execSQL(CREATE_CONTACTS_TABLE);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        //Drop older table if existed
        db.execSQL("DELETE TABLE IF EXISTS " + TABLE_CONTACTS);

        //Create tables again
        onCreate(db);
    }
    public long createContacts(holdContacts holdContacts) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(KEY_NAME, holdContacts.name); // Contact Name
        values.put(KEY_SURNAME, holdContacts.Surname); // Contact Surname
        values.put(KEY_PHONE, holdContacts.phoneNumber); // Contact Phone Number


        //Inserting a row
        long result = db.insert(TABLE_CONTACTS,null,values);
        return result;
    }

     // obtains the ContactList
    public ArrayList<holdContacts> getContactsList()
    {
        ArrayList<holdContacts> contactList = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_CONTACTS,null,null,null,null,null,null);
        cursor.moveToFirst();
        for(int i = 0; i < cursor.getCount(); i++)
        {
          //int id = cursor.getInt(cursor.getColumnIndex(KEY_ID));
          String firstName = cursor.getString(cursor.getColumnIndex(KEY_NAME));
          String Surname = cursor.getString(cursor.getColumnIndex(KEY_SURNAME));
          String phoneNumber = cursor.getString(cursor.getColumnIndex(KEY_PHONE));


            holdContacts holdContacts = new holdContacts(id,firstName,Surname,phoneNumber);
            contactList.add(holdContacts);
            cursor.moveToNext();

        }
        return contactList;

    }
    // delete the contacts
    public void deleteContacts(holdContacts holdContacts)
    {
        SQLiteDatabase db = getWritableDatabase();
        db.delete(TABLE_CONTACTS,KEY_ID +"=?",new String[]{String.valueOf(holdContacts.getName())});
        db.close();
    }


}
