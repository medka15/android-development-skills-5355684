package com.example.owner.mycontacts;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends ActionBarActivity {

    ListView listContacts;
    ArrayList<holdContacts> contactList;
    dbHelper dbHelper;
    private static final int EDIT = 0, DELETE =1;
    int longClickedItemsIndex;
    ArrayAdapter<holdContacts> contactAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listContacts = (ListView) findViewById(R.id.listView);
        dbHelper = new dbHelper(getApplicationContext());
        populateContactList();

        registerForContextMenu(listContacts);
        listContacts.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                longClickedItemsIndex = position;
                return false;

            }

        });
    }
    public void onCreateContextMenu(ContextMenu menu, View view, ContextMenu.ContextMenuInfo menuInfo)
    {
        super.onCreateContextMenu(menu, view, menuInfo);
        menu.setHeaderTitle("Options");
        menu.add(Menu.NONE,EDIT, Menu.NONE, "Edit Contacts");
        menu.add(Menu.NONE,DELETE, Menu.NONE, "Delete Contacts");

    }

    public boolean onContextItemSelected(MenuItem item)
    {
      switch (item.getItemId())
      {
          case EDIT:
              //TODO: Implement editing a contact
               break;

          case DELETE:
              dbHelper.deleteContacts(contactList.get(longClickedItemsIndex));
              contactList.remove(longClickedItemsIndex);
              contactAdapter.notifyDataSetChanged();
              break;

      }
        return super.onContextItemSelected(item);

    }

    public void populateContactList()
    {
        contactList = dbHelper.getContactsList();
        listContacts.setAdapter(new ContactAdapter());
    }

    class ContactAdapter extends BaseAdapter
    {
        @Override
        public int getCount() {
            return contactList.size();

        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            View layout = convertView;
            if(layout == null)
            {
              layout = getLayoutInflater().inflate(R.layout.item_contacts_list,parent,false);
            }

            TextView tvName = (TextView) layout.findViewById(R.id.textView4);
            TextView tvSurname = (TextView) layout.findViewById(R.id.textView5);
            TextView tvPhoneNumber= (TextView) layout.findViewById(R.id.textView6);


            holdContacts contacts = contactList.get(position);

            tvName.setText(contacts.name);
            tvSurname.setText(contacts.Surname);
            tvPhoneNumber.setText(contacts.phoneNumber);

            return layout;

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 100 && resultCode == RESULT_OK)
        {
            populateContactList();
            Toast.makeText(getApplicationContext(),"This Contact has been added", Toast.LENGTH_SHORT).show();


        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_add_contact)
        {
            startActivityForResult(new Intent(MainActivity.this, AddContacts.class), 100);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
