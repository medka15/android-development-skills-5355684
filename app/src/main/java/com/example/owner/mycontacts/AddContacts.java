package com.example.owner.mycontacts;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

// adds the contacts onto the list
public class AddContacts extends ActionBarActivity {

    EditText firstName;
    EditText surname;
    EditText phoneNumber;
    dbHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_contacts);
        firstName = (EditText) findViewById(R.id.firstName);
        surname = (EditText) findViewById(R.id.Surname);
        phoneNumber = (EditText) findViewById(R.id.phoneNumber);

        dbHelper = new dbHelper(getApplicationContext());
    }
     public void saveContacts(View view)
     {
      String fName = firstName.getText().toString().trim();
      String lastName = surname.getText().toString().trim();
      String phone = phoneNumber.getText().toString().trim();

      holdContacts values = new holdContacts(fName,lastName,phone);
      if(dbHelper.createContacts(values)!= -1)
      {
       setResult(RESULT_OK);
       finish();
      }

     }



    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add_contacts, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
