package com.example.mynotepad;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

public class NoteActivity extends Activity
{
	EditText notes;
	int position;
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);	//xml view for writing notes
		notes = (EditText) findViewById(R.id.notesField);	//link the EditText with xml
		notes.setBackgroundResource(R.drawable.textbcg);	//set background
		if(!MainActivity.Notesadapter.isEmpty() && getIntent().hasExtra("position")) //checks whether the adapter is empty or not, and if the adapter can be altered.
		{
			position = getIntent().getIntExtra("position", -1);
			notes.setText(MainActivity.Notesadapter.getItem(position).getNote().toString());
		}

	}
	public boolean onCreateOptionsMenu(Menu menu)
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
	    getActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#302013")));
		return true;
	}
	public boolean onOptionsItemSelected(MenuItem item)
	{
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		switch(id)
		{ 
		case R.id.action_settings:
			return true;
			
		case R.id.save_note :
			if(notes.toString().length() > 0)	//when the user types their notes, return what is typed
			{
				Intent intent = new Intent();
				intent.putExtra("notes", notes.getText().toString()); //data from notes
				intent.putExtra("position", position); 				  //position used to identify the altered notes
				setResult(RESULT_OK, intent); 
				finish(); // when the job is completed
			}
			break;
		}
		return true;
	}
		
}
