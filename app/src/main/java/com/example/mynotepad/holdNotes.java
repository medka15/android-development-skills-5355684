package com.example.mynotepad;

public class holdNotes
{
	  //Mutator and Accessor Methods...
	  private long id;	//id for SQLite Database(which is the primary key)
	  private String note;

	  public long getId()
	  {
	    return id;
	  }

	  public void setId(long id)
	  {
	    this.id = id;
	  }
	  
	  public String getNote()
	  {
	    return note;
	  }
	  public void setNote(String note)
	  {
	    this.note = note;
	  }

	  @Override
	  public String toString()
	  {
	    if(note.length() > 20)
	    	return note.substring(0, 20) + "...";
	    else 
			return note;
	  }
	} 