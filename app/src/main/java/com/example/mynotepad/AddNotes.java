package com.example.mynotepad;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

public class AddNotes
{
  // Database fields
  private SQLiteDatabase database;	//define SQL Database
  private com.example.mynotepad.dbHelper dbHelper;	//define database helper used to write to database
  private String[] allColumns = { com.example.mynotepad.dbHelper.COLUMN_ID, com.example.mynotepad.dbHelper.COLUMN_NOTE }; //database columns.

  public AddNotes(Context context)	//class constructor
  {
    dbHelper = new com.example.mynotepad.dbHelper(context);
  }

  public void open() throws SQLException
  {
    database = dbHelper.getWritableDatabase();
  }

  public void close()
  {
    dbHelper.close();
  }

  public holdNotes createNotes(String note)	//custom method to add to database
  {
    ContentValues values = new ContentValues();
    values.put(com.example.mynotepad.dbHelper.COLUMN_NOTE, note);	//add new note from argument passed
    long insertId = database.insert(com.example.mynotepad.dbHelper.TABLE_NOTES, null, values);	//id based on primary key
    Cursor cursor = database.query(com.example.mynotepad.dbHelper.TABLE_NOTES, allColumns, com.example.mynotepad.dbHelper.COLUMN_ID + " = " + insertId, null, null, null, null); //cursor to traverse database
    cursor.moveToFirst();
    holdNotes newNote = cursorToNote(cursor);
    cursor.close();
    return newNote;
  }

  public void deleteNotes(holdNotes note) //custom method to delete from database
  {
	long id = note.getId();
    database.delete(com.example.mynotepad.dbHelper.TABLE_NOTES, com.example.mynotepad.dbHelper.COLUMN_ID + " = " + id, null); //delete note from argument
  }

  public List<holdNotes> getAllNotes()
  {
    List<holdNotes> notes = new ArrayList<holdNotes>();
    Cursor cursor = database.query(com.example.mynotepad.dbHelper.TABLE_NOTES, allColumns, null, null, null, null, null);

    cursor.moveToFirst();
    while (!cursor.isAfterLast())	//recursively traverse all notes via cursor
    {
    	holdNotes note = cursorToNote(cursor);
    	notes.add(note);
    	cursor.moveToNext();
    }
    cursor.close();
    return notes;
  }
 
  private holdNotes cursorToNote(Cursor cursor)
  {
	holdNotes note = new holdNotes();
	note.setId(cursor.getLong(0));
	note.setNote(cursor.getString(1));
	return note;
  }
}