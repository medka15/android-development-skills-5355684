package com.example.mynotepad;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.List;

public class MainActivity extends Activity
{
	static ArrayAdapter<holdNotes> Notesadapter;	//define adapter for SQLite Database
	AddNotes sourcedata;						//SQLite data source
	ListView notesList;							//ListView to list all notes made by the user
    Intent intent;								//Define a global intent to start new activity

    @Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.notes_list);
		
		sourcedata = new AddNotes(this);		//Initialise the data
	    sourcedata.open();						//Open SQLite database
	    List<holdNotes> values = sourcedata.getAllNotes();		//Read database and assign to values of type List.

	    // use the SimpleCursorAdapter to show the
	    // elements in a ListView
	    Notesadapter = new ArrayAdapter<holdNotes>(this, android.R.layout.simple_list_item_1, values);	//initialise adapter
	    notesList = (ListView) findViewById(R.id.notesList);	//Link ListView with xml
	    notesList.setAdapter(Notesadapter);							//assign ListView's adapter.
	    notesList.setOnItemClickListener(new OnItemClickListener()	//OnClick Listener method to edit notes
	    {
			public void onItemClick(AdapterView<?> parent, View view, int position, long id)
			{
				intent = new Intent(MainActivity.this , NoteActivity.class);
				intent.putExtra("position", position);
				startActivityForResult(intent, 5);
			}
		});
	    notesList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener()	//OnLongClick Listener method to delete items
	    {
	        public boolean onItemLongClick(AdapterView<?> parent, View v, int position,long id)
	        {
	        	sourcedata.deleteNotes((holdNotes) notesList.getAdapter().getItem(position));
	        	Notesadapter.remove(Notesadapter.getItem(position));
	        	Notesadapter.notifyDataSetChanged();
				return true;
	        }
	    });
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
	    getActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#302013"))); //Set Actionbar color
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.

		int id = item.getItemId();
		switch(id)
		{
			case R.id.action_settings:
				return true;
				
			case R.id.add_note:
				intent = new Intent(MainActivity.this , NoteActivity.class);	//if user clicks on add note (in actionbar) start new activity
				startActivityForResult(intent, 0); //I always put 0 for someIntValue
                break;

		}
		return super.onOptionsItemSelected(item);
	}
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
	    super.onActivityResult(requestCode, resultCode, data);
	    if(requestCode == 0)		//0 for new request
	    {
		    if(data.getStringExtra("notes") != null)
		    {
			    String editTextValue = data.getStringExtra("notes");	//get data from second activity corresponding with notes string value
			    holdNotes currentNote = null;							//^ and set temp object to be queal to user input
				currentNote = sourcedata.createNotes(editTextValue);		//save temp object in the SQLite Database
			    Notesadapter.add(currentNote);								//add temp object to adapter
			    Notesadapter.notifyDataSetChanged();							//update ui
		    }
	    }
	    else if(requestCode == 5)	//5 for edit
	    {
	    	String editTextValue = data.getStringExtra("notes");	//...
		    holdNotes currentNote = null;							//...
			currentNote = sourcedata.createNotes(editTextValue);		//...
		    Notesadapter.add(currentNote);								//...
		    sourcedata.deleteNotes((holdNotes) notesList.getAdapter().getItem(data.getIntExtra("position", -1))); //delete old note from database
        	Notesadapter.remove(Notesadapter.getItem(data.getIntExtra("position", -1)));									  //delete old not from adapter
		   Notesadapter.notifyDataSetChanged();								//update ui
	    }
	}
}